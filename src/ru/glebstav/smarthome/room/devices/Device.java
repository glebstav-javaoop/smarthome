package ru.glebstav.smarthome.room.devices;

public abstract class Device{
  protected String name;
  protected Integer pin;

  public Device(String name, Integer pin){
    this.name = name;
    this.pin = pin;
  }

  public String getName() {
    return name;
  }
}
