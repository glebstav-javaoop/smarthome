package ru.glebstav.smarthome.exceptions;

import java.util.NoSuchElementException;

public class NoSuchDevice extends NoSuchElementException {
  static final String E_CANT_FIND = "Can't find device:";
  String device;
  public NoSuchDevice(String device){
    super();
    this.device = device;
  }
  @Override
  public String toString() {
    return E_CANT_FIND + device + super.toString();
  }
}
