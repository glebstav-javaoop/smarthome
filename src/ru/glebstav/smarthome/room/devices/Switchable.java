package ru.glebstav.smarthome.room.devices;

public interface Switchable {
  void switchDevice();
  void setDigitalValue(Boolean value);
}
