package ru.glebstav.smarthome.room.devices;

public interface Informational {
  Integer getAnalogValue();
  boolean getDigitalValue();
}
