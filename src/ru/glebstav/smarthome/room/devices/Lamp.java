package ru.glebstav.smarthome.room.devices;


public class Lamp extends Actuator  {

  public Lamp(String name, Integer pin){
    super(name,pin);
  }

  @Override
  public void switchDevice() {
    setDigitalValue(!status);
  }



  @Override
  public void setDigitalValue(Boolean value) {
    if(value!=status) {
      status = value;
    }
  }

}
