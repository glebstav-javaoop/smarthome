package ru.glebstav.smarthome;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.glebstav.smarthome.room.Room;

import java.util.Set;

public class TelegramBot extends TelegramLongPollingBot {
  static final String FIRST_MSG = "/start";
  static final String BOT_USER_NAME = "SmartHome_Gleb";
  static final String BOT_TOKEN = "1328328265:AAGEhLZpSR1MxllJDEGz_W_1ybYlgh81A4k";
  private Set<Room> rooms;
  private Reply reply;

  @Override
  public void onUpdateReceived(Update update) {
    // We check if the update has a message and the message has text
    if (update.hasMessage() && update.getMessage().hasText()) {
      SendMessage message = reply.processMessage(update.getMessage());

      try {
          execute(message); // Call method to send the message
      } catch (TelegramApiException e) {
        e.printStackTrace();
      }
    }
  }

  public void setReply(Reply reply) {
    this.reply = reply;
  }

  @Override
  public String getBotUsername() {
    return BOT_USER_NAME;
  }

  @Override
  public String getBotToken() {
    return BOT_TOKEN;
  }
}
