package ru.glebstav.smarthome;
import org.reflections.Reflections;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.glebstav.smarthome.room.Room;
import ru.glebstav.smarthome.room.devices.Actuator;
import ru.glebstav.smarthome.room.devices.Lamp;
import ru.glebstav.smarthome.room.devices.LightSensor;
import ru.glebstav.smarthome.room.devices.Sensor;

import java.util.*;

public class Reply {
  static final String UNKNOWN = "Unknown command";
  static final String START = "/start";
  static final String NEW_ROOM = "New room";
  static final String CREATE_NEW_ROOM = "Create new room";
  static final String SENSOR = "Add sensor";
  static final String ACTUATOR = "Add device";
  static final String CHOOSE_ACTUATOR = "Choose a device";
  static final String CHOOSE_SENSOR = "Choose a sensor";
  static final String CHECK = "Check status";
  static final String SENSOR_EDIT_CONF = "Set sensitivity";
  static final String PRINT_NAME_OF_ROOM = "Print name of your room";
  static final String ROOM_ADDED = "New room added";
  static final String ROOM_KITCHEN = "kitchen";
  static final String IS_ON = " is ON";
  static final String IS_OFF = " is OFF";
  static final String CHOOSE_SENS = "Choose sensitive";
  static final String SENS_CHANGED = ": sensitivity changed to ";
  private Message message;
  private SendMessage sendMessage;
  private Set<Room> rooms;
  private Set<String> messages;
  private Set<Class<? extends Actuator>> actuators;
  private Set<Class<? extends Sensor>> sensors;
  private Boolean replyStart;
  private Boolean replyNewRoomName = false;
  private Boolean replySensetive = false;
  private Room room;


  public Reply(){
    rooms = new HashSet<>();
    messages = new HashSet<>();
    getDevices();
    replyStart = false;

    room = new Room(ROOM_KITCHEN);         //This thing should have been created using a bot,
    room.addActuator(new Lamp("Lampa",12));                      // like a constructor
    room.addSensor(new LightSensor("sensorLight",4));
    room.setDependency("Lampa","sensorLight");

  }

  private void setButtons(Set<String> buttonWords){
    ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
    keyboardMarkup.setSelective(true);
    keyboardMarkup.setResizeKeyboard(true);
    keyboardMarkup.setOneTimeKeyboard(true);
    sendMessage.setReplyMarkup(keyboardMarkup);
    List<KeyboardRow> keyboard = new ArrayList<>();
    for(String word : buttonWords){
      KeyboardRow row = new KeyboardRow();
      row.add(new KeyboardButton(word));
      keyboard.add(row);
    }
    keyboardMarkup.setKeyboard(keyboard);
  }

  private void newRoom(String name){
    rooms.add(new Room(name));
  }

  private void getDevices(){
    actuators = (new Reflections("ru.glebstav.smarthome.room.devices")).getSubTypesOf(Actuator.class);
    sensors = (new Reflections("ru.glebstav.smarthome.room.devices")).getSubTypesOf(Sensor.class);

  }


  public SendMessage processMessage(Message message){
    Set<String> words = new HashSet<>();
    this.message = message;
    sendMessage = new SendMessage();
    sendMessage.setChatId(message.getChatId());

    if(message.getText().equals(CHECK)){
      if(room.statusOfActuator("Lampa")){
        sendMessage.setText("Lampa" + IS_ON);
      } else
        sendMessage.setText("Lampa" + IS_OFF);
      words.add(SENSOR_EDIT_CONF);
      words.add(CHECK);
      setButtons(words);
      return sendMessage;
    }
    if(message.getText().equals(SENSOR_EDIT_CONF)){
      sendMessage.setText(CHOOSE_SENS);
      for(Integer num = 0;num<=10;num++) {
        words.add(num.toString());
      }
      setButtons(words);
      replySensetive = true;
      return sendMessage;
    }
    if(replySensetive){
      replySensetive = false;
      room.setSensitivity("sensorLight",Integer.parseInt(message.getText())*100);
      sendMessage.setText("sensorLight"+SENS_CHANGED+message.getText());
      words.add(SENSOR_EDIT_CONF);
      words.add(CHECK);
      setButtons(words);
      return sendMessage;
    }


   /* if(message.getText().equals(START) && !replyStart){ //start
      replyStart = true;
      words.add(NEW_ROOM);
      setButtons(words);
      sendMessage.setText(CREATE_NEW_ROOM);
      return sendMessage;
    }
    if(message.getText().equals(NEW_ROOM)){  //creating new room
      replyNewRoomName = true;
      sendMessage.setText(PRINT_NAME_OF_ROOM);
      return sendMessage;
    }
    if(replyNewRoomName){  // name of room
      replyNewRoomName = false;
      newRoom(message.getText());
      sendMessage.setText(ROOM_ADDED);
      words.add(ACTUATOR);
      words.add(SENSOR);
      setButtons(words);
      return sendMessage;
    }
    if(message.getText().equals(ACTUATOR)){ //choose some actuator
      sendMessage.setText(CHOOSE_ACTUATOR);
      for(Class<? extends Actuator> actuator : actuators) {
        words.add(actuator.getSimpleName());
      }
      setButtons(words);
      return sendMessage;
    }
    if(message.getText().equals(SENSOR)){ //choose some sensor
      sendMessage.setText(CHOOSE_SENSOR);
      for(Class<? extends Actuator> actuator : actuators) {
        words.add(actuator.getSimpleName());
      }
      setButtons(words);
      return sendMessage;
    }*/




    words.add(SENSOR_EDIT_CONF);
    words.add(CHECK);
    setButtons(words);
    sendMessage.setText(UNKNOWN);
    return sendMessage;
  }





}
