package ru.glebstav.smarthome.room;

import ru.glebstav.smarthome.exceptions.*;
import ru.glebstav.smarthome.room.devices.Actuator;
import ru.glebstav.smarthome.room.devices.Device;
import ru.glebstav.smarthome.room.devices.Sensor;
import org.apache.log4j.*;
import java.util.*;

public class Room implements Runnable {
  static final Logger log = Logger.getLogger(Room.class);
  static final String COMMA = ",";
  static final String L_DEB_START_INIT = "Start of initialization room";
  static final String L_DEB_END_INIT = "End of initialization room";
  static final String L_INF_RUN = "Start of checking dependencies";
  static final String L_ERR_THR_SLEEP = "Thread can't sleep";
  static final String L_ERR_NO_SUCH_DEVICE = "Can't find device: ";
  static final String L_INF_SET_DEP = "Added dependencies between: ";
  static final String L_INF_ADD_ACT = "Added actuator: ";
  static final String L_INF_ADD_SENS = "Added sensor: ";
  static final String L_INF_SENS_CHANGED = ": sensitivity changed to ";
  Thread thread;
  private String name;
  private Set<Device> sensors;
  private Set<Device> actuators;
  private Map<Actuator, Sensor> dependency;

  public Room(String name){
    log.debug(L_DEB_START_INIT);
    this.name = name;
    sensors = new HashSet<>();
    actuators = new HashSet<>();
    dependency = new HashMap<>();
    thread = new Thread(this);
    thread.setDaemon(true);
    thread.start();
    log.debug(L_DEB_END_INIT);
  }

  public String getName(){
    return name;
  }

  public void addSensor(Sensor sensor){
    sensors.add(sensor);
    log.info(L_INF_ADD_SENS + sensor.getName());
  }
  public void addActuator(Actuator actuator){
    actuators.add(actuator);
    log.info(L_INF_ADD_ACT + actuator.getName());
  }


  private Device getDevice(String name, Set<Device> devices){
    for(Device device : devices)
      if(device.getName().equals(name))
        return device;
    log.error(L_ERR_NO_SUCH_DEVICE + name);
    throw new NoSuchDevice(name);
  }

  public boolean statusOfActuator(String name){
    return ((Actuator)getDevice(name,actuators)).getStatus();
  }

  public void setSensitivity(String nameOfSensor,int value){
    Sensor sensor = (Sensor)getDevice(nameOfSensor,sensors);
    log.info(nameOfSensor + L_INF_SENS_CHANGED + value);
    sensor.setAnalogValue(value);
  }


  public void setDependency(String nameOfActuator, String nameOfSensor){
    Actuator actuator = (Actuator)getDevice(nameOfActuator,actuators);
    Sensor sensor = (Sensor)getDevice(nameOfSensor,sensors);
    dependency.put(actuator,sensor);
    log.info(L_INF_SET_DEP + nameOfActuator + COMMA + nameOfSensor);
  }

  @Override
  public void run() {
    log.info(L_INF_RUN);
    do {

      for (Map.Entry<Actuator,Sensor> pair : dependency.entrySet()){
        pair.getKey().setDigitalValue(pair.getValue().getDigitalValue());
      }

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        log.error(L_ERR_THR_SLEEP,e);
        e.printStackTrace();
      }
    } while (true);
  }
}
