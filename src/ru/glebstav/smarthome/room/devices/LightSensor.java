package ru.glebstav.smarthome.room.devices;


public class LightSensor extends Sensor {
  static final Integer DEFAULT_THRESHOLD = 512;
  static final Integer TEST_VALUE = 600;
  public LightSensor(String name, Integer pin) {
    super(name, pin);
    threshold = DEFAULT_THRESHOLD;
  }

  @Override
  public Integer getAnalogValue() {
    return TEST_VALUE;
  }

  @Override
  public boolean getDigitalValue() {
    return getAnalogValue() > threshold;
  }

  @Override
  public void setAnalogValue(Integer threshold) {
    this.threshold = threshold;
  }
}
