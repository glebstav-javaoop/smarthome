package ru.glebstav.smarthome.room.devices;

public interface Tunable {
  void setAnalogValue(Integer value);
}
