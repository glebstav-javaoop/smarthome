package ru.glebstav.smarthome.room.devices;

public abstract class Actuator extends Device implements Switchable {
  protected boolean status;

  public Actuator(String name, Integer pin){
    super(name, pin);
    status = false;
  }

  public boolean getStatus(){
    return status;
  }
}
