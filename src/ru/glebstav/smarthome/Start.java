package ru.glebstav.smarthome;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.glebstav.smarthome.room.Room;

public class Start {
  public static void main(String[] args) {
    ApiContextInitializer.init();
    TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
    Reply reply = new Reply();
    TelegramBot HomeBot = new TelegramBot();
    HomeBot.setReply(reply);
    try {

      telegramBotsApi.registerBot(HomeBot);
    } catch (TelegramApiRequestException e) {
      e.printStackTrace();
    }

  }
}
