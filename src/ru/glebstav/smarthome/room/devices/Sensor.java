package ru.glebstav.smarthome.room.devices;

public abstract class Sensor extends Device implements Informational,Tunable{
  protected Integer value;
  protected Integer threshold;
  public Sensor(String name,Integer pin){
    super(name,pin);
  }

}
